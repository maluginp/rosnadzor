var express = require('express'), 
	pg      = require('pg'),
	util    = require('util'),
	fs      = require('fs'),
	logger  = require('./server/logger'),
	jade    = require('jade');

var app = express();

var config = require('./server/config');

var connectionString = 'postgres://'+config.db.user+':'+config.db.password
						+'@'+config.db.host+':'+config.db.port+'/'+config.db.database;

var db = new pg.Client(connectionString);
db.connect(function(err){
	if(err){
		return console.error('could not connect to db',err);
	}
});



app.configure( function(){
	app.use(express.static('./public/'));
	// app.use(express.logger());
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(function noCache(req, res, next){
    	res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    	res.header("Pragma", "no-cache");
    	res.header("Expires",0);
    	next();
	});
});

fs.readdirSync("./server/app/controllers").forEach(function(file) {
	console.log('Load controller:'+file)
  var LoadController = require("./server/app/controllers/" + file);
  new LoadController(app,db);
});


var jsFiles = [];

fs.readdirSync('./public/app/directives').forEach(function(file){
	jsFiles.push('app/directives/'+file);
});
fs.readdirSync('./public/app/models').forEach(function(file){
	jsFiles.push('app/models/'+file);
});
fs.readdirSync('./public/app/controllers').forEach(function(file){
	jsFiles.push('app/controllers/'+file);
});


var indexHtml = jade.renderFile('./server/app/views/index.jade',{
	'js_files' : jsFiles,
	'pageTitle' : 'База данных'
});

app.get('*',function(req,res){
	res.send(indexHtml);
});

app.listen(config.server.port,function(err){
	console.log('Start service on port ', config.server.port);
});
