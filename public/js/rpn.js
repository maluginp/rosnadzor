var app = angular.module('rpn',['ngRoute']);

app.config(function($routeProvider){
	$routeProvider
	// Объекты
	.when('/subjects',{
		controller: 'ListSubject',
		templateUrl: 'app/views/object/list.html'
	})
	.when('/object/:id/view',{
		controller: 'ViewObject',
		templateUrl: 'app/views/object/view.html'
	})
	.when('/object/:id/edit',{
		controller: 'ChangeObject',
		templateUrl: 'app/views/object/edit.html'
	})
	.when('/object/:subject_id/new',{
		controller: 'ChangeObject',
		templateUrl: 'app/views/object/edit.html'
	})
	.when('/object/:object_id/instruction/new',{
		controller: 'ChangeInstruction',
		templateUrl: 'app/views/instruction/edit.html'
	})
	.when('/object/:object_id/instruction/:id/view',{
		controller: 'ViewInstruction',
		templateUrl: 'app/views/instruction/view.html'
	})
	.when('/object/:object_id/instruction/:id/edit',{
		controller: 'ChangeInstruction',
		templateUrl: 'app/views/instruction/edit.html'
	})
	.when('/object/:object_id/instruction/:instruction_id/act/new',{
		controller: 'ChangeAct',
		templateUrl: 'app/views/act/edit.html'
	})
	.when('/object/:object_id/instruction/:instruction_id/act/:id/view',{
		controller: 'ViewAct',
		templateUrl: 'app/views/act/view.html'
	})
	.when('/object/:object_id/instruction/:instruction_id/act/:id/edit',{
		controller: 'ChangeAct',
		templateUrl: 'app/views/act/edit.html'
	})
	.when('/object/:object_id/instruction/:instruction_id/act/:act_id/protocol/new',{
		controller: 'ChangeProtocol',
		templateUrl: 'app/views/protocol/edit.html'
	})
	.when('/object/:object_id/instruction/:instruction_id/act/:act_id/protocol/:id/view',{
		controller: 'ViewProtocol',
		templateUrl: 'app/views/protocol/view.html'
	})
	.when('/object/:object_id/instruction/:instruction_id/act/:act_id/protocol/:id/edit',{
		controller: 'ChangeProtocol',
		templateUrl: 'app/views/protocol/edit.html'
	})
	.when('/documents',{
		controller: 'ListDocument',
		templateUrl: 'app/views/document/list.html'
	})
	.when('/document/new',{
		controller: 'ChangeDocument',
		templateUrl: 'app/views/document/edit.html'
	})
	.when('/document/:id/edit',{
		controller: 'ChangeDocument',
		templateUrl: 'app/views/document/edit.html'
	})
	.when('/document/:id/view',{
		controller: 'ViewDocument',
		templateUrl: 'app/views/document/view.html'
	})
	.when('/statistic',{
		controller: 'ViewStatistic',
		templateUrl: 'app/views/statistic/view.html'
	})
	.otherwise({
		redirectTo:'/subjects'
	});
});

app.filter('localdate', function() {
    return function(input, format) {
        return moment(parseInt(input)).utc().format(format);
    };
});
