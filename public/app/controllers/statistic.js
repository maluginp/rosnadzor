angular.module('rpn')
.controller('ViewStatistic',function($http,$scope,$routeParams,$location,ActModel,ObjectModel,ProtocolModel){

	var localScope = {};

	localScope.isLoaded = false;
	
	localScope.period = {
		// from : new Date().getTime(),
		from : 1388534400000,
		to   : new Date().getTime()
	}

	

	localScope.objects = {

	};

	localScope.enums = {
		object_place   : ObjectModel.enumPlaces,
		object_business : ObjectModel.enumBusiness,
		act_place : ActModel.enumPlaces,
		event : ActModel.enumEvents,
		type : ActModel.enumTypes,
		reason : ActModel.enumReasons,
		control : ActModel.enumControls,
		protocol_type : ProtocolModel.enumTypes,
	};

	localScope.show = function(){
		console.log('From:',$scope.period.from,'To:',$scope.period.to);

		$http.get('/api/statistics/objects/'+$scope.period.from+'/'+$scope.period.to)
		.success(function(subjects){
			$scope.subjects = subjects;
			$scope.number_objects = 0;
			subjects.forEach(function(subject){
				$scope.number_objects += subject.objects.length;
			});
			// $scope.isLoaded = true;		
			console.log('Subjects stat',subjects);
		})
		.error(function(){
			
		});


		$http.get('/api/statistics/acts/'+$scope.period.from+'/'+$scope.period.to)
		.success(function(acts){
			$scope.acts = acts;
			// $scope.isLoaded = true;		
			console.log('Acts stat',acts);
		})
		.error(function(){
			
		});


		$http.get('/api/statistics/protocols/'+$scope.period.from+'/'+$scope.period.to)
		.success(function(protocols){
			$scope.protocols = protocols;
				
			console.log('Protocols stat',protocols);
		})
		.error(function(){
			
		});

		$scope.isLoaded = true;	

	}
	

	angular.extend($scope,localScope);

});