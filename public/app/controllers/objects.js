angular.module('rpn')
.controller('ListSubject',function($scope,$location,ObjectModel){
	$scope.objects = [];

	$scope.isSubject = true;

	$scope.enums = {
		place : ObjectModel.enumPlaces,
		business : ObjectModel.enumBusiness
	};

	ObjectModel.list(-1)
	.then(function(data){
		$scope.objects = data;
	});

	$scope.view = function(objectId){
		$location.path('/object/'+objectId+'/view');
	}

	$scope.add = function(){
		$location.path('/object/-1/new');
	}

})
.controller('ViewObject',function($scope,$routeParams,$location,ObjectModel,InstructionModel){
	var id = $routeParams.id;
	
	if(angular.isUndefined(id) || id <= 0){
		return $location.path('/subjects');
	}

	$scope.object = {};
	$scope.objects = [];
	$scope.instructions = [];
	$scope.isSubject = false;

	$scope.enums = {
		place : ObjectModel.enumPlaces,
		business : ObjectModel.enumBusiness
	};

	ObjectModel.get(id)
	.then(function(data){
		$scope.object = data;
		$scope.isSubject = ($scope.object.subject_id === -1);
	})
	.then(function(){
		if($scope.isSubject){
			ObjectModel.list($scope.object.id)
			.then(function(objects){
				$scope.objects = objects;
			});
		}else{
			ObjectModel.get($scope.object.id)
			.then(function(data){
				$scope.object.subject = data;
			})
		}
	})
	.then(function(){
		InstructionModel.list($scope.object.id)
		.then(function(instructions){
			$scope.instructions = instructions;

			console.log("Found instructions",instructions);
		})
	});


	$scope.viewObject = function(objectId){
		$location.path('/object/'+objectId+'/view');
	}

	$scope.edit = function(){
		$location.path('/object/'+id+'/edit');
	}

	$scope.remove = function(){
		var confirmRemove = confirm('Вы уверены что хотите удалить?');
		if(confirmRemove){
			ObjectModel.remove(id)
			.then(function(){
				if($scope.isSubject){
					$location.path('/subjects');
				}else{
					$location.path('/object/'+$scope.object.subject_id+'/view')
				}
			});
		}
	}

	$scope.add = function(){
		if($scope.isSubject){
			return $location.path('/object/'+id+'/new');
		}
	}

	$scope.addInstruction = function(){
		$location.path('/object/'+id+'/instruction/new');
	}

	$scope.viewInstruction = function(instructionId){
		$location.path('/object/'+id+'/instruction/'+instructionId+'/view');	
	}
})
.controller('ChangeObject',function($scope,$routeParams,$location,ObjectModel){
	var id = $routeParams.id;
	var subjectId = $routeParams.subject_id || -1;
	$scope.isNew = angular.isDefined(subjectId) && angular.isUndefined(id);
	$scope.isSubject = (subjectId == -1);

	$scope.enums = {
		place : ObjectModel.enumPlaces,
		business : ObjectModel.enumBusiness
	};


	if($scope.isNew){
		$scope.object = ObjectModel.new(subjectId);
	}else{
		ObjectModel.get(id)
		.then(function(data){
			$scope.object = data;
			$scope.isSubject = $scope.object.subject_id == -1;
		});
	}

	$scope.change = function(){
		console.log('Применяем следующий объект',$scope.object);

		ObjectModel.set($scope.object)
		.then(function(){
			console.log('Измения объекта приняты');
			if($scope.isNew){
				if( subjectId === -1 ){
					$location.path('/subjects');
				}else{
					$location.path('/object/'+subjectId+'/view');
				}
			}else{
				$location.path('/object/'+id+'/view');
			}
		})
		.catch(function(){
			console.error('Измения объекта не приняты');
		});
	}

	$scope.back = function(){
		if($scope.isSubject){
			$location.path('/subjects');
		}else{
			$location.path('/object/'+subjectId+'/view');
		}
	}
});