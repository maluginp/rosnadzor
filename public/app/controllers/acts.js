angular.module('rpn')
.controller('ChangeAct',function($scope,$routeParams,$location,ActModel){
	var id = $routeParams.id;
	var objectId = $routeParams.object_id;
	var instructionId = $routeParams.instruction_id;

	$scope.enums = {
		places : ActModel.enumPlaces,
		events : ActModel.enumEvents,
		types  : ActModel.enumTypes,
		reasons : ActModel.enumReasons,
		controls : ActModel.enumControls
	};
	
	$scope.isNew = angular.isUndefined(id);

	$scope.act = ActModel.new(instructionId);

	if(!$scope.isNew){
		ActModel.get( id )
		.then(function(act){
			$scope.act = act;
		});
	}

	$scope.change = function(){
		ActModel.set( $scope.act )
		.then(function(){
			console.log('Акт изменен');
			if($scope.isNew){
				$location.path('/object/'+objectId+'/instruction/'+instructionId+'/view');
			}else{
				$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+id+'/view');
			}
		})
		.catch(function(){
			console.error('Акт не изменен');
		})
	}

	$scope.back = function() {
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/view');
	}
})
.controller('ViewAct',function($scope,$routeParams,$location,ActModel,InstructionModel, ProtocolModel,DocumentModel){
	var id = $routeParams.id;
	var objectId = $routeParams.object_id;
	var instructionId = $routeParams.instruction_id;

	$scope.enums = {
		places : ActModel.enumPlaces,
		events : ActModel.enumEvents,
		types  : ActModel.enumTypes,
		reasons : ActModel.enumReasons,
		protocolTypes : ProtocolModel.enumTypes,
		controls : ActModel.enumControls
	}

	$scope.protocols = [];

	$scope.act = {};

	ActModel.get(id)
	.then(function(act){
		$scope.act = act;
		InstructionModel.get(instructionId)
		.then(function(instruction){
			$scope.act.instruction = instruction;
		})
	})
	.then(function(){
		ProtocolModel.list(id)
		.then(function(protocols){	
			$scope.protocols = [];	
			// var protocol = {};
			protocols.forEach(function(protocol){
				ActModel.get(protocol.act_id)
				.then(function(act){
					protocol.act = act;
				})

				DocumentModel.get(protocol.document_id)
				.then(function(doc){
					protocol.document = doc;
				});

				$scope.protocols.push(protocol);
			});

		})
	});

	$scope.view = function(actId){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/view');
	}
	$scope.edit = function(){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+id+'/edit');	
	}
	$scope.addProtocol = function(){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+id+'/protocol/new');
	}
	$scope.viewProtocol = function(protocolId){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+id+'/protocol/'+protocolId+'/view');
	}

	$scope.remove = function(){
		var confirmRemove = confirm('Вы уверены что хотите удалить?');
		if(confirmRemove){
			ActModel.remove(id)
			.then(function(){
				$location.path('/object/'+objectId+'/instruction/'+instructionId+'/view');
			});
		}
	}
	$scope.viewArticle = function(protocolId,articleId){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'
			+id+'/protocol/'+protocolId+'/article/'+articleId+'/view');
	}

});
