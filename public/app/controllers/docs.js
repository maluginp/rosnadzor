angular.module('rpn')
.controller('ChangeDocument',function($scope,$routeParams,$location,DocumentModel){
	var id = $routeParams.id;
	$scope.isNew = angular.isUndefined(id);

	DocumentModel.get(id)
	.then(function(doc){
		$scope.doc = doc;
	})

	$scope.change = function(){
		DocumentModel.set( $scope.doc )
		.then(function(){
			console.log('Документ изменён');
			$location.path('/documents');
		})
		.catch(function(){
			console.log('Документ не изменён');
		})
	}	
})
.controller('ViewDocument',function($scope,$routeParams,$location,DocumentModel){
	var id = $routeParams.id;
	
	$scope.doc = {};

	DocumentModel.get(id)
	.then(function(doc) {
		$scope.doc = doc;
	});

	$scope.edit = function() {
		$location.path('/document/'+id+'/edit');
	}

	
})
.controller('ListDocument',function($scope,$location,DocumentModel){
	$scope.documents = {};

	DocumentModel.list()
	.then(function(docs) {
		$scope.documents = docs;
	});

	$scope.edit = function(id) {
		$location.path('/document/'+id+'/edit');
	}
	$scope.view = function(id) {
		$location.path('/document/'+id+'/view');
	}
	$scope.add = function(){
		$location.path('/document/new');
	}
	
});