angular.module('rpn')
.controller('ChangeInstruction',function($scope,$routeParams,$location,InstructionModel){
	var id = $routeParams.id;
	var objectId = $routeParams.object_id;

	var today = new Date().getTime();
	$scope.isNew = angular.isUndefined(id);

	$scope.instruction = {
		id: -1,
		number: '',
		object_id : objectId,
		started: new Date().getTime(),
		ended: new Date().getTime()
	}

	if(!$scope.isNew){
		InstructionModel.get( id )
		.then(function(instruction){
			$scope.instruction = instruction;
		});
	}

	$scope.change = function(){
		if($scope.instruction.started > $scope.instruction.ended){
			return alert('Начальная дата больше конечной');
		}

		InstructionModel.set( $scope.instruction )
		.then(function(){
			console.log('Распоряжение изменено');
			if($scope.isNew){
				$location.path('/object/'+objectId+'/view');
			}else{
				$location.path('/object/'+objectId+'/instruction/'+id+'/view');
			}
		})
		.catch(function(){
			console.error('Распоряжение не изменено');
		})
	}
})
.controller('ViewInstruction',function($scope,$routeParams,$location,InstructionModel,ObjectModel,ActModel){
	var id = $routeParams.id;
	var objectId = $routeParams.object_id;

	$scope.instruction = {};

	InstructionModel.get(id)
	.then(function(instruction){
		$scope.instruction = instruction;

		console.log('View instruction',instruction);
		ObjectModel.get(objectId)
		.then(function(object){
			$scope.instruction.object = object;
		})

		ActModel.getByInstructionId(id)
		.then(function(act){
			$scope.instruction.act = act;
		})
		.catch(function(){
			$scope.instruction.act = {id:-1};
		})
	})
	$scope.view = function(instructionId){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/view');
	}
	$scope.edit = function(){
		$location.path('/object/'+objectId+'/instruction/'+id+'/edit');	
	}
	$scope.add = function(){
		$location.path('/object/'+objectId+'/instruction/new');	
	}
	$scope.remove = function(){
		var confirmRemove = confirm('Вы уверены что хотите удалить?');
		if(confirmRemove){
			InstructionModel.remove(id)
			.then(function(){
				$location.path('/object/'+objectId+'/view');
			});
		}
	}

});
