angular.module('rpn')
.controller('ChangeProtocol',function($scope,$routeParams,$location,ProtocolModel,DocumentModel){
	var id = $routeParams.id;
	var objectId = $routeParams.object_id;
	var instructionId = $routeParams.instruction_id;
	var actId = $routeParams.act_id;
// 
	
	$scope.enums = {
		types : ProtocolModel.enumTypes
	}
	$scope.isNew = angular.isUndefined(id);

	$scope.documents = [{id:-1,name:'Без документа',description:''}];

	DocumentModel.list()
	.then(function(docs){
		$scope.documents = $scope.documents.concat(docs);
	});

	$scope.protocol = ProtocolModel.new(actId);
		

	if(!$scope.isNew){
		ProtocolModel.get( id )
		.then(function(protocol){
			$scope.protocol = protocol;
		});
	}

	$scope.change = function(){
		ProtocolModel.set( $scope.protocol )
		.then(function(){
			console.log('Протокол изменен');
			// if($scope.isNew){
				$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/view');
			// }else{
				// $location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/protocol/'+id+'/view');
			// }
		})
		.catch(function(){
			console.error('Протокол не изменен');
		})
	}

	$scope.back = function(){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/view');
	}
})
.controller('ViewProtocol',function($scope,$routeParams,$location,ProtocolModel,ActModel,DocumentModel){
	var id = $routeParams.id;
	var objectId = $routeParams.object_id;
	var instructionId = $routeParams.instruction_id;
	var actId = $routeParams.act_id;

	$scope.protocol = {};

	$scope.enums = {
		types : ProtocolModel.enumTypes
	}

	ProtocolModel.get(id)
	.then(function(protocol){
		$scope.protocol = protocol;
		ActModel.get(actId)
		.then(function(act){
			$scope.protocol.act = act;
		})

		DocumentModel.get(protocol.document_id)
		.then(function(doc){
			$scope.protocol.document = doc;
		});

	})

	$scope.view = function(protocolId){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/protocol/'+protocolId+'/view');
	}
	$scope.edit = function(){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/protocol/'+id+'/edit');
	}
	$scope.add = function(){
		$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/protocol/new');
	}
	$scope.remove = function(){
		var confirmRemove = confirm('Вы уверены что хотите удалить?');
		if(confirmRemove){
			ProtocolModel.remove(id)
			.then(function(){
				$location.path('/object/'+objectId+'/instruction/'+instructionId+'/act/'+actId+'/view');
			});
		}
	}

});
