// Модель: Акт
// Один акт может принадлежать одному распоряжению
angular.module('rpn')
.service('ActModel',function($http,$q){

	this.get = function(id){
		var d = $q.defer();

		if(angular.isDefined(id) || id > 0){
			$http.get('/api/act/'+id)
			.success(function(act){
				d.resolve(act);
			})
			.error(function(){
				d.reject();
			})
		}else{
			d.reject();
		}
		return d.promise;
	}

	this.getByInstructionId = function(instructionId){
		var d = $q.defer();

		if(angular.isDefined(instructionId) || instructionId > 0){
			$http.get('/api/act/by-instruction/'+instructionId)
			.success(function(act){
				d.resolve(act);
			})
			.error(function(){
				d.reject();
			})
		}else{
			d.reject();
		}

		return d.promise;
	}

	this.set = function(act){
		var d = $q.defer();

		if(act.event == 'plan'){
			act.reason = '';
		}

		$http.post('/api/acts',{'act':act})
		.success(function(result){
			if(result){
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(){
			return d.reject();
		})

		return d.promise;
	}

	this.remove = function(id){
		var d = $q.defer();

		if(angular.isDefined(id) || id > 0){
			$http.delete('/api/act/'+id)
			.success(function(result){
				if(result){
					return d.resolve();
				}
				return d.reject();
			})
			.error(function(){
				return d.reject();
			});
		}else{
			d.reject();
		}

		return d.promise;
	}

	this.enumEvents = {
		'plan'   : 'Плановая',
		'unplan' : 'Внеплановая'
	};

	this.enumTypes = {
		'seb' : 'СЭБ',
		'zpp' : 'ЗПП',
		'seb-zpp':' СЭБ и ЗПП'
	};

	this.enumPlaces = {
		'market' : 'Торговля',
		'catering' : 'Общепит',
		'industry' : 'Пищепром'
	};

	this.enumReasons = {
		'instruction' : 'Предписание',
		// 'appeal' : 'Жалоба',
		'threat'      : 'Возникновения угрозы причинения вреда жизни, здоровью граждан',
		'harm'        : 'Причинения вреда жизни, здоровью граждан',
		'violation'   : 'Нарушения прав потребителей',
		'president'   : 'Указ президента',
		'uprn'        : 'Приказы УПРН'
	};

	this.enumControls = {
		'product' : 'Видам реализуемой продукции',
		'noise'   : 'Замерам шума и вибрации',
		'state'   : 'Неудовлетворительному сан-гиг. состоянию.'
	};

	this.new = function(instructionId){
		return {
			id: -1,
			instruction_id : instructionId, 
			created : new Date().getTime(),
			event : 'plan',
			type : 'seb-zpp',
			place : 'market',
			reason : '',
			order_number : 0,
			order_till: new Date().getTime(),
			order_done: false,
			lab_used: false,
			control : 'product',
			control_description : ''
		};
	}

// информации о фактах возникновения угрозы причинения вреда жизни, здоровью граждан;
// информации о фактах причинения вреда жизни, здоровью граждан;
// нарушения прав потребителей (в случае обращения граждан, права которых нарушены).

});