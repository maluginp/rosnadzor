// Модель: Распоряжение
angular.module('rpn')
.service('InstructionModel',function($http,$q){

	this.get = function(id){
		var d = $q.defer();

		$http.get('/api/instruction/'+id)
		.success(function(instruction){
			// Hooks, change
			d.resolve(instruction);
		})
		.error(function(){
			d.reject();
		})

		return d.promise;
	}

	this.set = function(instruction){
		var d = $q.defer();

		$http.post('/api/instructions', {'instruction' : instruction})
		.success(function(result){
			if(result){
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(result){
			return d.reject();
		});

		return d.promise;
	}

	this.remove = function(id){
		var d = $q.defer();
		$http.delete('/api/instruction/'+id)
		.success(function(result){
			if(result){
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(result){
			return d.reject();
		});

		return d.promise;
	}

	this.list = function(objectId){
		var d = $q.defer();

		$http.get('/api/instructions/'+objectId)
		.success(function(instructions){
			console.log('Result instruction::list',instructions);
			return d.resolve(instructions);
		})
		.error(function(){
			return d.reject();
		})

		return d.promise;
	}
});