// Модель: Объект
angular.module('rpn')
.service('ObjectModel',function($http,$q){
	
	this.get = function(id) {
		var d = $q.defer();

		if(id === undefined || id === -1){
			d.reject();
			return d.promise;
		}

		$http.get('/api/object/'+id).success(function(data){
			d.resolve(data);
		}).error(function(data){
			d.reject();
		});

		return d.promise;
	}

	this.list = function(subjectId) {
		var d = $q.defer();

		$http.get('/api/objects/'+subjectId).success(function(data){
			d.resolve(data);
		}).error(function(data){
			d.reject();
		});

		return d.promise;
	}

	this.set = function(object){
		var d = $q.defer();

		if(angular.isString(object.name) && object.name === ''){
			d.reject();
			return d.promise;
		}

		if(angular.isString(object.address) && object.address === ''){
			d.reject();
			return d.promise;
		}

		$http.post('/api/objects',{'object':object})
		.success(function(result){
			if(result) {
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(data){
			return d.reject();
		});

		return d.promise;
	}


	this.remove = function(id){
		var d = $q.defer();

		$http.delete('/api/object/'+id)
		.success(function(result){
			if(result){
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(){
			return d.reject();
		});

		return d.promise;
	}

	this.enumPlaces = {
		'city' : 'Город',
		'district' : 'Район'
	};

	this.enumBusiness = {
		'big' : 'Крупный',
		'middle' : 'Средний',
		'small' : 'Малый'
	}

	this.new = function(subjectId){

		var object = {
			id : -1,
			subject_id : subjectId,
			name : '',
			place : 'city',
			address : '',
			inn : '',
			ogrn : '',
			business : ''	
		};

		if(subjectId == -1) {	
			
			object.business = 'small';
		}

		return object; 

	}

});