// Модель: Протокол
angular.module('rpn')
.service('ProtocolModel',function($http,$q){

	this.get = function(id){
		var d = $q.defer();

		$http.get('/api/protocol/'+id)
		.success(function(protocol){
			d.resolve(protocol);
		})
		.error(function(){
			d.reject();
		})

		return d.promise;
	}

	this.set = function(protocol){
		var d = $q.defer();

		$http.post('/api/protocols', {'protocol' : protocol})
		.success(function(result){
			if(result){
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(result){
			return d.reject();
		});

		return d.promise;
	}

	this.remove = function(id){
		var d = $q.defer();
		$http.delete('/api/protocol/'+id)
		.success(function(result){
			if(result){
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(result){
			return d.reject();
		});

		return d.promise;
	}

	this.list = function(actId){
		var d = $q.defer();

		$http.get('/api/protocols/'+actId)
		.success(function(protocols){
			return d.resolve(protocols);
		})
		.error(function(){
			return d.reject();
		})

		return d.promise;
	}

	this.enumTypes = {
		'official' : 'Должностное лицо',
		'legal' : 'Юридическое лицо',
		'individual' : 'Индивидуальный предприниматель'
	};


	this.new = function(actId){
		return {id: -1,
			act_id: actId,
			type: 'official',
			document_id: -1,
			summa: 0.00,
			send_court: false,
			recv_court: false,
			recv_court_description: '',
			number_violation:0
		}
	}
});