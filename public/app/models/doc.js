// Модель: Акт
// Один акт может принадлежать одному распоряжению
angular.module('rpn')
.service('DocumentModel',function($http,$q){

	this.get = function(id){
		var d = $q.defer();

		if(angular.isDefined(id) || id > 0){
			$http.get('/api/document/'+id)
			.success(function(doc){
				d.resolve(doc);
			})
			.error(function(){
				d.reject();
			})
		}else{
			d.reject();
		}
		return d.promise;
	}

	this.list = function(){
		var d = $q.defer();

		$http.get('/api/documents')
		.success(function(docs){
			d.resolve(docs);
		})
		.error(function(){
			d.reject();
		})
	
		return d.promise;
	}

	this.set = function(doc){
		var d = $q.defer();


		$http.post('/api/documents',{'doc':doc})
		.success(function(result){
			if(result){
				return d.resolve();
			}
			return d.reject();
		})
		.error(function(){
			return d.reject();
		})

		return d.promise;
	}

	this.remove = function(id){
		var d = $q.defer();

		if(angular.isDefined(id) || id > 0){
			$http.delete('/api/document/'+id)
			.success(function(result){
				if(result){
					return d.resolve();
				}
				return d.reject();
			})
			.error(function(){
				return d.reject();
			});
		}else{
			d.reject();
		}

		return d.promise;
	}
});