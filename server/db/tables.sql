﻿DROP TABLE objects;
DROP TYPE type_object;

CREATE TYPE type_object AS ENUM('city','district');
CREATE TABLE objects(
id    SERIAL PRIMARY KEY,
subject_id integer DEFAULT -1,
name varchar(255) NOT NULL,
type type_object,
address varchar(255)
);

-- Добавим тестовые данные

-- Субъекты
INSERT INTO objects(name,type,address) VALUES ('Субъект1','city','Адрес город первого субъекта'),
                                              ('Субъект2','district','Адрес район второго субъекта'),
                                              ('Субъект3','city','Адрес город третьего субъекта');

-- 1,2,3
-- SELECT * FROM objects;

-- Создаём объекты для Субъекта 1
INSERT INTO objects(name,type,address,subject_id) VALUES ('Объект1','city','Адрес объекта 1',1),
                                                         ('Объект2','district','Адрес объекта 2',1),
                                                         ('Объект3','city','Адрес объекта 3',1),
                                                         ('Объект4','city','Адрес объекта 4',2),
                                                         ('Объект5','city','Адрес объекта 5',2),
                                                         ('Объект6','district','Адрес объекта 6',3),
                                                         ('Объект7','city','Адрес объекта 7',2),
                                                         ('Объект8','city','Адрес объекта 8',3),
                                                         ('Объект9','district','Адрес объекта 9',3),
                                                         ('Объект10','city','Адрес объекта 10',3);
              

SELECT * FROM objects;                                           