-- Создать необходимы типы и таблицы
CREATE TYPE ACT_EVENTS     AS ENUM('plan','unplan');
CREATE TYPE ACT_TYPES      AS ENUM('seb','zpp','seb-zpp');
CREATE TYPE ACT_PLACES     AS ENUM('market','catering','industry');
CREATE TYPE ACT_REASONS    AS ENUM('','instruction','appeal','president');
CREATE TYPE PROTOCOL_TYPES AS ENUM('official','legal','individual');
CREATE TYPE PLACE_OBJECT   AS ENUM('city','district');

CREATE TABLE objects(
	id    SERIAL PRIMARY KEY,
	name varchar(255) NOT NULL,
	subject_id INTEGER DEFAULT -1,
	place PLACE_OBJECT,
	address VARCHAR(255),
    inn VARCHAR(100),
    ogrn VARCHAR(100),
    business VARCHAR(100)
);

CREATE TABLE instructions(
    id SERIAL PRIMARY KEY,
    number VARCHAR(255),
    object_id INTEGER REFERENCES objects(id) ON DELETE CASCADE,
    started BIGINT,
    ended BIGINT
);

CREATE TABLE acts(
    id SERIAL PRIMARY KEY,
    instruction_id INTEGER REFERENCES instructions(id) ON DELETE CASCADE,
    created BIGINT,
    event ACT_EVENTS,
    type ACT_TYPES,
    place ACT_PLACES,
    reason VARCHAR(100) DEFAULT '',
    order_number BIGINT,
    order_till BIGINT,
    order_done BOOLEAN,
    lab_used BOOLEAN,
    control VARCHAR(100) DEFAULT '',
    control_description TEXT DEFAULT ''
);

CREATE TABLE protocols(
    id SERIAL PRIMARY KEY,
    act_id INTEGER REFERENCES acts(id) ON DELETE CASCADE,
    document_id INTEGER,
    type PROTOCOL_TYPES,
    summa NUMERIC(12,2) DEFAULT 0.00,
    send_court BOOLEAN DEFAULT FALSE,
    recv_court BOOLEAN DEFAULT FALSE,
    recv_court_description TEXT DEFAULT '',
    number_violation INTEGER
);


CREATE TABLE documents(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    description TEXT
);


--- Обновление
ALTER TABLE acts ADD COLUMN order_number BIGINT,
                 ADD COLUMN order_till BIGINT,
                 ADD COLUMN order_done BOOLEAN DEFAULT false;

ALTER TABLE acts ALTER COLUMN reason TYPE VARCHAR(100);
ALTER TABLE acts ALTER COLUMN reason SET DEFAULT '';

ALTER TABLE acts ADD COLUMN lab_used BOOLEAN DEFAULT false;
ALTER TABLE protocols ADD COLUMN number_violation INTEGER DEFAULT 0;

ALTER TABLE objects ADD COLUMN inn VARCHAR(100) DEFAULT '',
                 ADD COLUMN ogrn VARCHAR(100) DEFAULT '',
                 ADD COLUMN business VARCHAR(100) DEFAULT '';

ALTER TABLE acts ADD COLUMN control VARCHAR(100) DEFAULT '',
                 ADD COLUMN control_description TEXT DEFAULT '';