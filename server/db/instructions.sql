﻿-- -- Предписание (instruction)
-- DROP TABLE instructions;
-- DROP TABLE acts;
-- DROP TABLE articles;
-- DROP TABLE protocol_articles;
-- DROP TABLE protocols;
-- DROP TYPE ACT_EVENTS;
-- DROP TYPE ACT_TYPES;
-- DROP TYPE ACT_PLACES;
-- DROP TYPE ACT_REASONS;
-- DROP TYPE PROTOCOL_TYPES;

CREATE TYPE ACT_EVENTS  AS ENUM('plan','unplan');
CREATE TYPE ACT_TYPES   AS ENUM('seb','zpp','seb-zpp');
CREATE TYPE ACT_PLACES  AS ENUM('market','catering','industry');
CREATE TYPE ACT_REASONS AS ENUM('','instruction','appeal','president');
CREATE TYPE PROTOCOL_TYPES AS ENUM('official','legal','individual');


CREATE TABLE instructions(
    id SERIAL PRIMARY KEY,
    digit INTEGER,
    object_id INTEGER,
    started DATE,
    ended DATE,
    act_id INTEGER DEFAULT -1
);

CREATE TABLE acts(
    id SERIAL PRIMARY KEY,
    created DATE,
    event ACT_EVENTS,
    type ACT_TYPES,
    place ACT_PLACES,
    reason ACT_REASONS DEFAULT ''
);

INSERT INTO acts(created,event,type,place,reason) VALUES
    ('2014-01-23','unplan','seb','market','appeal'),
    ('2014-02-01','plan','seb-zpp','market','');

INSERT INTO instructions(digit,object_id,started,ended,act_id) VALUES
    (10,5,'2014-01-20','2014-01-27',1),
    (15,1,'2014-01-28','2014-02-03',2);


CREATE TABLE articles(
    id SERIAL PRIMARY KEY,
    digit VARCHAR(10),
    name VARCHAR(255),
    description TEXT
);

INSERT INTO articles(digit,name,description) VALUES
    ( '2.2', 'Статья первая', 'Описание статьи' ),
    ( '2.3', 'Статья вторая', 'Описание статьи' );


CREATE TABLE protocol_articles(
    id SERIAL PRIMARY KEY,
    article_id INTEGER,
    summa MONEY,
    send_court BOOLEAN DEFAULT FALSE,
    recv_court BOOLEAN DEFAULT FALSE,
    recv_court_description TEXT DEFAULT ''
);

INSERT INTO protocol_articles(article_id,summa,send_court) VALUES
    (1,100.00,TRUE),
    (1,50.00,FALSE),
    (2,100.00,FALSE);


CREATE TABLE protocols(
    id SERIAL PRIMARY KEY,
    act_id INTEGER,
    type PROTOCOL_TYPES,
    article_indexes INTEGER[]
);

INSERT INTO protocols(act_id,type,article_indexes) VALUES
    (1,'legal','{1,3}'),
    (1,'individual','{2}');

