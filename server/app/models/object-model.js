// @model Object
// @column id
// @column name
// @column subject_id
// @column place
// @column address

var Q = require('q');

var ObjectModel = function(db){
	this.set = function( object ){
		var d = Q.defer();

		if(object === undefined){
			d.reject( false, '' );
		}

		var isNew = (object.id === undefined || object.id  === -1);


		var rowItems = [
			object.name,
			object.subject_id,
			object.place,
			object.address,
			object.inn,
			object.ogrn,
			object.business
		];


		var callback = function(err,result){
			if(err){
				return d.reject(err);
			}
		 	return d.resolve();
		}

		if( isNew ){
			db.query('INSERT INTO objects(name,subject_id,place,address,inn,ogrn,business) \
					 VALUES($1,$2,$3,$4,$5,$6,$7)',rowItems,callback);		
		}else{
			rowItems.push(object.id)

			db.query('UPDATE objects SET name=$1,subject_id=$2,place=$3,address=$4,inn=$5,ogrn=$6,business=$7 \
					 WHERE id=$8',rowItems,callback);	
		}

		return d.promise;
	}

	this.get = function(id) {
		var d = Q.defer();

		if(id === undefined){
			return d.reject( 'ID неопределён' );
		}
		if(db === undefined){
			return d.reject(' DB инициализирована');
		}

		db.query('SELECT * FROM objects WHERE id=$1',[id],function(err,result){

			if(err){
				return d.reject(err);
			}

			if(result.rows.length !== 1){
				return d.reject('');
			}
			var object = result.rows[0];

			return d.resolve( object );

		});


		return d.promise;
	}

	this.delete = function(id){
		var d = Q.defer();

		if(id === undefined){
			return d.reject( 'ID неопределён' );
		}

		db.query('DELETE FROM objects WHERE id=$1 OR subject_id=$1',[id],function(err,result){
			if(err){
				return d.reject(err);
			}

			return d.resolve( );

		});


		return d.promise;
	}


	this.listBySubjectId = function( subjectId ){
		var d = Q.defer();

		console.log('Object::listBySubjectId('+subjectId+')')

		if(subjectId === undefined){
			d.reject( 'subjectId неопределён' );
		}

		db.query('SELECT * FROM objects WHERE subject_id=$1',[subjectId],function(err,result){
			if(err){ return d.reject(err); }
			var rows = result.rows.length;
			console.log('> Found',rows,'objects');

			return d.resolve( result.rows );
		});


		return d.promise;
	}
}

module.exports = ObjectModel;