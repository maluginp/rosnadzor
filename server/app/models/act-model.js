// @model Act
// @column id
// @column instruction_id
// @column created
// @column event
// @column type
// @column place
// @column reason

// Предписание
// @column order_number
// @column order_till
// @column order_done


var Q = require('q');

module.exports = function(db){

	this.set = function(act){
		var d = Q.defer();

		if(act === undefined){
			d.reject( false, '' );
		}

		var isNew = (act.id === undefined || act.id  === -1);

		var items = [ 
					  act.instruction_id,
					  act.created,
					  act.event,
					  act.type,
					  act.place,
					  act.reason,
					  act.order_number,
					  act.order_till,
					  act.order_done,
					  act.lab_used,
					  act.control,
					  act.control_description
					];

		if(!isNew){
			items.push( act.id );
		}


		if( isNew ){
			db.query('INSERT INTO acts(instruction_id,created,event,type,place,reason, \
									   order_number,order_till,order_done,lab_used, \
									   control, control_description) \
					 			VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)',items,function(err,result) {

					 	if(err){
					 		return d.reject(err);
					 	}

					 	return d.resolve();
			});		
		}else{
			db.query('UPDATE acts SET instruction_id=$1,created=$2,event=$3,type=$4,place=$5, \
							 reason=$6,order_number=$7,order_till=$8, order_done=$9, lab_used=$10, \
							 control=$11, control_description=$12 WHERE id=$13',items,function(err,result) {
					 	if(err){
					 		return d.reject(err);
					 	}

					 	return d.resolve();
			});	
		}

		return d.promise;
	}

	this.get = function(id){
		var d = Q.defer();

		if(id === undefined){
			d.reject( 'ID неопределён' );
		}

		db.query('SELECT * FROM acts WHERE id=$1',[id],function(err,result){

			if(err){
				return d.reject(err);
			}

			if(result.rows.length !== 1){
				return d.reject('');
			}
			var object = result.rows[0];

			return d.resolve( object );

		});


		return d.promise;
	}

	this.delete = function(id){
		var d = Q.defer();

		if(id === undefined){
			return d.reject( 'ID неопределён' );
		}

		db.query('DELETE FROM acts WHERE id=$1',[id],function(err,result){

			if(err){
				return d.reject(err);
			}

			return d.resolve( );

		});


		return d.promise;
	}

	this.getByInstructionId = function(instructionId){
		var d = Q.defer();
		console.log('Act::getByInstructionId('+instructionId+')');
		if(instructionId === undefined){
			return d.reject( 'instructionId неопределён' );
		}

		db.query('SELECT * FROM acts WHERE instruction_id=$1',[instructionId],function(err,result){
			var rows = result.rows.length;
			if(err){
				return d.reject(err);
			}

			var act = {id:-1};
			if(rows != 1){
				return d.reject('');
			}

			act = result.rows[0];

			return d.resolve( act );

		});


		return d.promise;
	}
}