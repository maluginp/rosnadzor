// @model Document
// @column id
// @column name
// @column description

var Q = require('q');

var DocumentModel = function(db){

	this.set = function(doc){
		var d = Q.defer();

		if(doc === undefined){
			d.reject('Document object is undefined');
			return d.promise;
		}

		var isNew = (doc.id === undefined || doc.id === -1);

		if(isNew){
			db.query('INSERT INTO documents(name,description) VALUES($1,$2)',
				[doc.name,doc.description],function(err,result){

				if(err){
					return d.reject(err);
				}

				return d.resolve();
			});

		}else{
			db.query('UPDATE documents SET name=$1,description=$2 WHERE id=$3',
				[doc.name,doc.description,doc.id],function(err,result){

				if(err){
					return d.reject(err)
				}
				return d.resolve();
			});
		}

		return d.promise;
	}

	this.get = function(id){
		var d = Q.defer();

		if(id === undefined){
			d.reject('Document Id is undefined');
			return d.promise;
		}

		db.query('SELECT * FROM documents WHERE id=$1',[id],function(err,result){
			if(err){
				return d.reject(err);
			}

			if(result.rows.length !== 1){
				return d.reject('');
			}
			var doc = result.rows[0];

			return d.resolve( doc );
		})

		return d.promise;
	}

	this.delete = function(id){
		var d = Q.defer();

		if(id === undefined){
			d.reject( 'ID неопределён' );
			return d.promise;
		}

		db.query('DELETE FROM documents WHERE id=$1',[id],function(err,result){

			if(err){
				return d.reject(err);
			}

			return d.resolve();
		});

		return d.promise;
	}

	this.list = function(){
		var d = Q.defer();
		
		db.query('SELECT * FROM documents',function(err,result){
			if(err){
				return d.reject(err);
			}
			var docs = result.rows;
			return d.resolve(docs);
		})

		return d.promise;
	}
}

module.exports = DocumentModel;