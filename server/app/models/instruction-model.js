// @model Instruction
// @column id
// @column number
// @column object_id
// @column started
// @column ended

var Q = require('q');
var async = require('async');

var ActModel = require('./act-model');

var InstructionModel = function(db){
	'use strict';
	this.set = function(instruction){
		var d = Q.defer();

		if(instruction === undefined){
			d.reject( false, '' );
		}

		var isNew = (instruction.id === undefined || instruction.id  === -1);

		if( isNew ){
			db.query('INSERT INTO instructions(number,object_id,started,ended) \
					 VALUES($1,$2,$3,$4)',[instruction.number,instruction.object_id,
					 instruction.started,instruction.ended],function(err,result) {

					 	if(err){
					 		return d.reject(err);
					 	}

					 	return d.resolve();
			});		
		}else{
			db.query('UPDATE instructions SET number=$1,object_id=$2,started=$3,ended=$4\
					 WHERE id=$5',[instruction.number,instruction.object_id,instruction.started,
					 instruction.ended,instruction.id],function(err,result) {

					 	if(err){
					 		return d.reject(err);
					 	}

					 	return d.resolve();
			});	
		}

		return d.promise;
	}

	this.get = function(id){
		var d = Q.defer();
		var act = new ActModel(db);

		if(id === undefined){
			d.reject( 'ID неопределён' );
		}

		db.query('SELECT * FROM instructions WHERE id=$1',[id],function(err,result){
			if(err){
				return d.reject(err);
			}

			if(result.rows.length !== 1){
				return d.reject('');
			}
			var instruction = result.rows[0];

			act.getByInstructionId( id )
			.then(function(_act){
				instruction.act = _act;
			})
			.catch(function(err){
				instruction.act = {id : -1};
			})
			.done(function(){
				d.resolve( instruction );
			})
			
		});

		return d.promise;
	}

	this.delete = function(id){
		var d = Q.defer();

		if(id === undefined){
			d.reject( 'ID неопределён' );
		}

		db.query('DELETE FROM instructions WHERE id=$1',[id],function(err,result){
			if(err){
				return d.reject(err);
			}

			return d.resolve();
		});


		return d.promise;
	}

	this.listByObjectId = function (objectId){
		console.log('listByObjectId(',objectId,')');
		var d = Q.defer();
		if(objectId === undefined){
			d.reject( 'Object ID неопределён' );
		}

		var self = this;

		db.query('SELECT * FROM instructions WHERE object_id=$1',[objectId],function(err,result){
			if(err){
				return d.reject(err);
			}

			var rows = result.rows.length;

			console.log('> Found ',rows,' instructions');

			async.map(result.rows,self.getActByInstructionId,function(err,instructions){
				if(instructions.length == rows){
					d.resolve(instructions);
				}
			});
			
		});

		return d.promise;
	}


	this.getActByInstructionId = function(row,callback){
		var actModel = new ActModel(db);

		var instruction = row;
		actModel.getByInstructionId( instruction.id )
		.then( function(_act){
			instruction.act = _act;
		})
		.catch(function(){
			instruction.act = {id:-1};
		})
		.done(function(){
			callback(null,instruction);
		});
	}
}

module.exports = InstructionModel;