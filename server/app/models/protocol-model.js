// @model Protocol
// @column id
// @column act_id 
// @column type

var Q = require('q');
var async = require('async');

var ProtocolModel = function(db){	

	this.set = function(protocol){
		var d = Q.defer();

		if(protocol === undefined){
			d.reject( false, '' );
		}

		var isNew = (protocol.id === undefined || protocol.id  === -1);

		var items = [ protocol.act_id, protocol.type, protocol.document_id, protocol.summa,
		              protocol.send_court,protocol.recv_court,
		              protocol.recv_court_description, protocol.number_violation  ];
		if(!isNew){
			items.push( protocol.id );
		}

		if( isNew ){
			db.query('INSERT INTO protocols(act_id,type,document_id,summa,send_court, \
					 recv_court,recv_court_description,number_violation) VALUES($1,$2,$3,$4,$5,$6,$7,$8)',items,function(err,result) {
				if(err){
					return d.reject(err);
				}
				return d.resolve();
			});		
		}else{
			db.query('UPDATE protocols SET act_id=$1, type=$2,document_id=$3,summa=$4,send_court=$5, \
					 recv_court=$6,recv_court_description=$7,number_violation=$8 WHERE id=$9',items,function(err,result) {
				if(err){
					return d.reject(err);
				}
				return d.resolve();
			});	
		}

		return d.promise;
	}

	this.get = function(id){
		var d = Q.defer();

		if(id === undefined){
			return d.reject( 'ID неопределён' );
		}

		db.query('SELECT * FROM protocols WHERE id=$1',[id],function(err,result){

			if(err){
				return d.reject(err);
			}

			if(result.rows.length !== 1){
				return d.reject('');
			}
			var protocol = result.rows[0];

			return d.resolve( protocol );

		});


		return d.promise;
	}

	this.delete = function(id){
		var d = Q.defer();

		if(id === undefined){
			return d.reject( 'ID неопределён' );
		}

		db.query('DELETE FROM protocols WHERE id=$1',[id],function(err,result){
			if(err){
				return d.reject(err);
			}

			return d.resolve( );
		});

		return d.promise;
	}

	this.listByActId = function(actId){
		var d = Q.defer();
		

		console.log('Act::listByActId('+actId+')');
		if(actId === undefined){
			return d.reject( 'Act ID неопределён' );
		}

		db.query('SELECT * FROM protocols WHERE act_id=$1',[actId],function(err,result){
			var rows = result.rows.length;
			console.log('> Found',rows,'protocols');
			if(err){
				return d.reject(err);
			}

			return d.resolve( result.rows );
		});

		return d.promise;
	}
}
module.exports = ProtocolModel;