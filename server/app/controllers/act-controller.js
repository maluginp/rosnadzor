var ActModel = require('../models/act-model');
var InstructionModel = require('../models/instruction-model');

var ActController = function(app,db){
	var model = new ActModel(db);
	var instruction = new InstructionModel(db);
	// Получить объект
	this.get = function(req,res) {
		var id = req.params.id;
		var act = {};

		model.get(id)
		.then(function(_act){
			act = _act;
			act.instruction = {};
		})
		.then(function(){
			instruction.get(act.instruction_id)
			.then(function(_instruction){
				act.instruction = _instruction;
			});
		})
		.catch(function(err){
			console.log('Error get',err);
		})
		.done(function(){
			res.json(act);
		});

	}

	// Создать/изменить объект
	this.post = function(req,res) {
		var act = req.body.act;
		var changed = true;

		model.set( act )
		.catch(function(err){
			console.log('Error in post', err);
			changed = false;
		})
		.done(function(){
			res.json(changed);
		})
	}

	// удалить объект
	this.delete = function(req,res) {
		var id = req.params.id;
		var deleted = true;

		model.delete(id)
		.catch(function(err){
			console.log('Error in post', err);
			deleted = false;
		})
		.done(function(){
			res.json(deleted);
		});
	}

	this.getByInstructionId = function(req,res){
		var instructionId = req.params.instruction_id;
		var act = {};
		model.getByInstructionId(instructionId)
		.then(function(_act){
			act = _act;
		})
		.catch(function(err){
			console.log('Error getByInstructionId',err);
		})
		.done(function(){
			res.json(act);
		});
	}

	app.post(   '/api/acts', this.post );
	app.get(    '/api/act/:id', this.get );
	app.get(    '/api/act/by-instruction/:instruction_id', this.getByInstructionId);
	app.delete( '/api/act/:id',this.delete );


}

module.exports = ActController;