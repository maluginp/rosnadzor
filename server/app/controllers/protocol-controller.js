var ProtocolModel = require('../models/protocol-model');

var ProtocolController = function(app,db){
	var model = new ProtocolModel(db);

	this.get = function(req,res) {
		var id = req.params.id;
		var protocol = {};

		model.get(id)
		.then(function(_protocol){
			protocol = _protocol;
			protocol.instruction = {};
		})
		.then(function(){
			instruction.get(protocol.instruction_id)
			.then(function(_instruction){
				protocol.instruction = _instruction;
			});
		})
		.catch(function(err){
			console.log('Error get',err);
		})
		.done(function(){
			res.json(protocol);
		});

	}

	// Создать/изменить объект
	this.post = function(req,res) {
		var protocol = req.body.protocol;
		var changed = true;

		model.set( protocol )
		.catch(function(err){
			console.log('Error in post', err);
			changed = false;
		})
		.done(function(){
			res.json(changed);
		})
	}

	// удалить объект
	this.delete = function(req,res) {
		var id = req.params.id;
		var deleted = true;

		model.delete(id)
		.catch(function(err){
			console.log('Error in post', err);
			deleted = false;
		})
		.done(function(){
			res.json(deleted);
		});
	}

	this.listByActId = function(req,res){
		var actId = req.params.act_id;
		var protocol = {};
		model.listByActId(actId)
		.then(function(_protocol){
			protocol = _protocol;
		})
		.catch(function(err){
			console.log('Error getByActId',err);
		})
		.done(function(){
			res.json(protocol);
		});
	}

	app.post(   '/api/protocols', this.post );
	app.get(    '/api/protocol/:id', this.get );
	app.get(    '/api/protocols/:act_id', this.listByActId);
	app.delete( '/api/protocol/:id',this.delete );
}

module.exports = ProtocolController;