var ObjectModel = require('../models/object-model');

var ObjectController = function( app, db ){
	console.log('Initialization ObjectController');

	var model = new ObjectModel(db);

	// Объекты по субъектам
	this.listBySubjectId = function(req,res){
		console.log('API Object.listBySubjectId');
		var subjectId = req.params.subject_id;
		var objects = [];

		model.listBySubjectId(subjectId)
		.then(function(_objects){
			objects = _objects;
		})
		.catch(function(err){
			console.log('Error listBySubjectId:',err);
		})
		.done(function(){
			res.json( objects );
		})
	}

	// Получить объект
	this.get = function(req,res){
		var id = req.params.id;
		var object = {};

		model.get(id)
		.then(function(_object){
			object = _object;
		})
		.then(function(){
			model.get( object.subject_id )
			.then(function(_subject){
				object.subject = _subject; 
			})
		})
		.catch(function(err){
			console.log('Error get',err);
		})
		.done(function(){
			res.json(object);
		});
	}

	// Создать/изменить объект
	this.post = function(req,res){
		var object = req.body.object;
		var changed = true;

		model.set( object )
		.catch(function(err){
			console.log('Error in post', err);
			changed = false;
		})
		.done(function(){
			res.json(changed);
		})
	}

	// удалить объект
	this.delete = function(req,res){
		var id = req.params.id;
		var deleted = true;

		model.delete(id)
		.catch(function(err){
			console.log('Error in post', err);
			deleted = false;
		})
		.done(function(){
			res.json(deleted);
		});
	}

	app.post(   '/api/objects', this.post );
	app.get(    '/api/object/:id', this.get );
	app.delete( '/api/object/:id',this.delete );
	// app.get(    '/api/objects',this.list);
	app.get(    '/api/objects/:subject_id',this.listBySubjectId )


}


module.exports = ObjectController;
