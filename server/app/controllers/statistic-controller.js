var async = require('async');
var Q = require('q');
var ObjectModel = require('../models/object-model');
var DocumentModel = require('../models/doc-model');

var StatisticController = function(app,db){
	'use strict';
	var self_ = this;

	var modelObject = new ObjectModel(db);
	var modelDocument = new DocumentModel(db);

	this.objects= function(req,res) {
		var from = req.params.from;
		var to   = req.params.to;

		console.log('> From',from,'To',to);
		
		var callback = function(err,result){
			if(err){
				console.log('Error db',err);
				res.json([]);
			}else{
				self_.handle_objects(result.rows)
				.then(function(stat){
					res.json(stat);	
				})					
			}
		}

		db.query('SELECT objects.* \
				  FROM objects,acts,instructions \
				  WHERE \
				  	objects.id=instructions.object_id AND instructions.id=acts.instruction_id \
				  	AND acts.created >= $1 AND acts.created <= $2 AND objects.subject_id=-1 \
   			  	  ORDER BY acts.created ASC',
				[from,to],callback);

	}

	this.handle_objects = function(objects){
		var d = Q.defer();


		var handle_object = function(subject,callback) {
			var isSubject = subject.subject_id == -1;

			if( isSubject ){
				var objects = [];

				modelObject.listBySubjectId(subject.id)
				.then(function(_objects){
					objects = _objects;
				})
				.done(function(){

					var subjectArray = {
						'subject' : subject,
						'objects' : objects
					};

					callback(null,subjectArray);

				})

			}

			// callback(null);
		}

		async.map( objects, handle_object, function(err,subjects){
			d.resolve(subjects);
		});


		return d.promise;
	}

	this.acts = function(req,res) {
		var from = req.params.from;
		var to   = req.params.to;

		var callback = function(err,result){
			if(err){
				console.log('Error db',err);
				res.json([]);
			}else{
				res.json(result.rows);			
			}
		}

		db.query('SELECT acts.*, instructions.number AS instruction_number, \
			      objects.id AS object_id, objects.name AS object_name \
				  FROM acts,instructions,objects \
				  WHERE acts.instruction_id = instructions.id AND \
				  	    objects.id = instructions.object_id	  AND \
				        acts.created >= $1 AND acts.created <= $2',
				[from,to],callback);


	}


	this.protocols = function(req,res) {
		var from = req.params.from;
		var to   = req.params.to;

		console.log('API statistics/protocols');
		var callback = function(err,result){
			if(err){
				console.log('Error db',err);
				res.json([]);
				return;
			}

			self_.handle_protocols(result.rows)
			.then(function(protocols){
				console.log('Send protocols',protocols);
				res.json(protocols);
			});

		}

		db.query('SELECT protocols.*, \
				         acts.place AS act_place, objects.id AS object_id, \
				         objects.name AS object_name \
				  FROM acts,protocols, instructions, objects \
				  WHERE protocols.act_id = acts.id AND \
				        acts.instruction_id = instructions.id AND \
				        instructions.object_id = objects.id AND \
				        acts.created >= $1 AND acts.created <= $2',
				[from,to],callback);

	}

	this.handle_protocols = function(protocols) {
		var d = Q.defer();


		var handle_protocol = function(protocol,callback) {
			
			if(protocol.document_id == -1){
				protocol['doc_id'] = -1;
				protocol['doc_name'] = '';
				callback(null,protocol);
			}else{
				modelDocument.get(protocol.document_id)
				.then(function(doc){
					protocol['doc_id'] = doc.id;
					protocol['doc_name'] = doc.name;
					callback(null,protocol);
				});
			}
		}

		async.map( protocols, handle_protocol, function(err,_protocols){
			d.resolve(_protocols);
		});


		return d.promise;
	}

	app.get('/api/statistics/objects/:from/:to',   this.objects);
	app.get('/api/statistics/acts/:from/:to',      this.acts);
	app.get('/api/statistics/protocols/:from/:to', this.protocols);


}

module.exports = StatisticController;