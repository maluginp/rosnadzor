var InstructionModel = require('../models/instruction-model');
var ObjectModel = require('../models/object-model');
var ActModel = require('../models/act-model');


var InstructionController = function(app,db){

	var model  = new InstructionModel(db);
	var act    = new ActModel(db);
 	var object = new ObjectModel(db);

	// Получить объект
	this.get = function(req,res){
		var id = req.params.id;
		var instruction = {};

		model.get(id)
		.then(function(_instruction){
			instruction = _instruction;
			instruction.object = {};
		})
		.then(function(){
			instruction.act = {};
			act.getByInstructionId(instruction.id)
			.then(function(_act){
				instruction.act = _act;
			});
		})
		.then(function(){
			object.get(instruction.object_id)
			.then(function(_object){
				instruction.object = _object;
			});
		})
		.catch(function(err){
			console.log('Error get',err);
		})
		.done(function(){
			res.json(instruction);
		});

	}

	// Создать/изменить объект
	this.post = function(req,res){
		var instruction = req.body.instruction;
		var changed = true;

		model.set( instruction )
		.catch(function(err){
			console.log('Error in post', err);
			changed = false;
		})
		.done(function(){
			res.json(changed);
		})
	}

	// удалить объект
	this.delete = function(req,res){
		var id = req.params.id;
		var deleted = true;

		model.delete(id)
		.catch(function(err){
			console.log('Error in post', err);
			deleted = false;
		})
		.done(function(){
			res.json(deleted);
		});
	}

	// список распоряжений по object_id
	this.getByObjectId = function(req,res){
		var objectId = req.params.object_id;
		var instructions = [];
		model.listByObjectId(objectId)
		.then(function(_instructions){
			instructions = _instructions;

			console.log('Result Instruction::getByObjectId',instructions);
		})
		.done(function(){
			res.json(instructions);
		})
	}

	app.post(   '/api/instructions', this.post );
	app.get(    '/api/instruction/:id', this.get );
	app.delete( '/api/instruction/:id',this.delete );	
	app.get(    '/api/instructions/:object_id',this.getByObjectId);

}

module.exports = InstructionController;