var DocumentModel = require('../models/doc-model');

var DocumentController = function(app,db){
	var model = new DocumentModel(db);

	this.get = function(req,res){
		var id = req.params.id;
		var doc = {};

		model.get(id)
		.then(function(_doc){
			doc = _doc;
		})
		.catch(function(err){
			console.log('Error get',err);
		})
		.done(function(){
			res.json(doc);
		})

	}

	this.post = function(req,res) {
		var doc = req.body.doc;
		var changed = true;

		model.set( doc )
		.catch(function(err){
			console.log('Error in post', err);
			changed = false;
		})
		.done(function(){
			res.json(changed);
		})
	}

	this.delete = function(req,res) {
		var id = req.params.id;
		var deleted = true;

		model.delete(id)
		.catch(function(err){
			console.log('Error in post', err);
			deleted = false;
		})
		.done(function(){
			res.json(deleted);
		});
	}

	this.list = function(req,res){
		var documents = [];
		model.list()
		.then(function(_documents){
			documents = _documents;
		})
		.done(function(){
			res.json(documents);
		})
	}
 	
 	app.post(   '/api/documents', this.post );
	app.get(    '/api/document/:id', this.get );
	app.get(    '/api/documents', this.list);
	app.delete( '/api/document/:id',this.delete );
}

module.exports = DocumentController;